# Programming Environment Change on Cori and Edison in April 2019

## Background

Following the scheduled maintenances on Cori (April 10) and Edison
(April 17), we will install the new Cray Programming Environment
Software release
[CDT/19.03](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_19.03_March_7_2019/resource/Released_Cray_XC_Programming_Environments_19.03_March_7_2019.pdf),
and retire old
[CDT/17.09](https://pubs.cray.com/bundle/Released_Cray_XC_Programming_Environments_17.09_September_7_2017/resource/Released_Cray_XC_Programming_Environments_17.09_September_7_2017.pdf).
New Intel compiler version 19.0.3.199 will also be installed. There
will be no software default versions change this time.

Below is the detailed list of changes after the scheduled maintenance
on Cori (April 10) and Edison (April 17).

## Software versions to be removed

* cce/8.6.2
* cray-fftw/3.3.6.2
* cray-hdf5, cray-hdf5-parallel/1.10.0.3
* cray-libsci/17.09.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.6.2
* cray-netcdf, cray-netcdf-hdf5parallel/4.4.1.1.3
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.7.6.0
* cray-python/17.09.1
* cray-tpsl, cray-tpsl-64/17.06.1
* cray-trilinos/12.10.1.1
* craype/2.5.12
* craypkg-gen/1.3.5
* gcc/7.1.0
* papi/5.5.1.3
* perftools, perftools-base, perftools-lite/6.5.2
* pmi, pmi-lib/5.0.12

## New software versions available

* cce/8.7.9
* cray-fftw/3.3.8.2
* cray-ga/5.3.0.10
* cray-libsci/19.02.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.6
* craype/2.5.18
* papi/5.6.0.6
* perftools, perftools-base, perftools-lite/7.0.6
* stat/3.0.1.3
