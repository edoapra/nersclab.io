#!/bin/bash
#SBATCH -N 2 
#SBATCH -C knl
#SBATCH -q regular
#SBATCH -t 6:00:00

module load vasp/6.3.2-knl
export OMP_NUM_THREADS=4
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

# launching 1 task every 4 cores (16 CPUs)
srun -n32 -c16 --cpu_bind=cores vasp_std

